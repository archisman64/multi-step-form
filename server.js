const container = document.querySelector('.container');

container.addEventListener('click', handleClick);

// create an object to store submission values
const obj = {
    'name': null,
    'email': null,
    'phone': null,
    'plan-name': null,
    'plan-type': 'monthly',
    'plan-price': null,
    'online-service': null,
    'larger-storage': null,
    'customizable-profile': null
};

function handleClick(event) {
    if (event.target.id === "toggleMonthlyYearly") {
        const toggleMonthlyYearlyCheckbox = document.getElementById('toggleMonthlyYearly');

        toggleMonthlyYearlyCheckbox.addEventListener('change', () => {
            resetPlanCardsAndTheirSelections();
            const checked = document.getElementById('toggleMonthlyYearly').checked;
            updatePlanDisplay(checked);
        })
    } else if (event.target.className === "add-on-check") {
        // handle styling of add-on cards
        if (event.target.checked) {
            event.target.parentElement.parentElement.classList.add('active');
        } else {
            event.target.parentElement.parentElement.classList.remove('active');
        }
    } else if (event.target.id === 'plan-change-link') {
        // reset the previously selected values to default values
        resetPlanCardsAndTheirSelections();
        resetAddOnCards();

        // show the plan selection form
        hideForm(document.querySelector('.summary'));
        showForm(document.querySelector('.select-plan'));
    } else {
        // console.log(event.target.type);
        event.preventDefault();
    };
    if (event.target.className === 'submit-btn-1') {
        handleInfoSubmission();
    } else if (event.target.className === 'submit-btn-2') {
        // check if plan has been selected and then proceed to next form
        document.getElementById('plan-error').textContent = '';

        // for next form
        // change default monthly prices to yearly if plan-type is yearly
        if (obj["plan-type"] === 'yearly') {
            document.getElementById('add-on-price-1').innerText = '$10/yr';
            document.getElementById('add-on-price-2').innerText = '$20/yr';
            document.getElementById('add-on-price-3').innerText = '$20/yr';
        } else {
            document.getElementById('add-on-price-1').innerText = '$1/mo';
            document.getElementById('add-on-price-2').innerText = '$2/mo';
            document.getElementById('add-on-price-3').innerText = '$2/mo';
        }

        handlePlanSubmission();
    } else if (event.target.className === 'submit-btn-3') {
        handleAddOnSubmission();
    } else if (event.target.className === 'submit-btn-4') {
        handleSummaryConfirmation();
    } else if (event.target.id === 'back-btn-1') {

        hideForm(document.querySelector('.select-plan'));
        showForm(document.querySelector('.your-info'));

    } else if (event.target.id === 'back-btn-2') {

        hideForm(document.querySelector('.add-ons'));
        showForm(document.querySelector('.select-plan'));

    } else if (event.target.id === 'back-btn-3') {

        hideForm(document.querySelector('.summary'));
        showForm(document.querySelector('.add-ons'));

    } else if (event.target.className === 'plan-card') {
        // make the plan error disappear
        document.getElementById('plan-error').textContent = '';

        // remove active class from all plan cards
        document.querySelectorAll('.plan-card').forEach((card) => {
            card.classList.remove('active');
        })
        // add active class to currently clicked card
        event.target.classList.add('active');

        const selectedPlan = document.getElementById(event.target.id);
        console.log('selected plans:', selectedPlan.children[1].children[0].innerText);
        if (obj["plan-type"] === 'monthly') {
            // if monthly plan is chosen
            obj["plan-name"] = selectedPlan.children[1].children[0].innerText;
            obj["plan-price"] = selectedPlan.children[1].children[1].innerText;
            console.log('monthly price set to:', obj['plan-price']);
        } else {
            // if yearly plan is chosen
            obj["plan-name"] = selectedPlan.children[1].children[0].innerText;
            obj["plan-price"] = selectedPlan.children[1].children[2].innerText;
            console.log('yearly price set to:', obj['plan-price']);
        }
        console.log(obj);
    }
}

const plans = document.querySelectorAll('.plan-card');

function resetAddOnCards() {
    document.querySelectorAll('.add-on-check').forEach((checkbox) => {
        checkbox.checked = false;
        checkbox.parentElement.parentElement.classList.remove('active');
    })
}

function resetPlanCardsAndTheirSelections() {
    obj["plan-price"] = null;
    obj["online-service"] = null;
    obj["larger-storage"] = null;
    obj["customizable-profile"] = null;

    document.querySelectorAll('.plan-card').forEach((card) => {
        card.classList.remove('active');
    })
}

function handleSummaryConfirmation() {
    const summaryForm = document.querySelector('.summary');
    const endMessage = document.querySelector('.submission-msg');

    // hide the current form
    hideForm(summaryForm);
    // show next form
    showForm(endMessage);
}

function updateSummary() {
    document.getElementById('main-plan').innerText = `${obj["plan-name"]} (${obj["plan-type"]})`;
    document.getElementById('main-price').innerText = `${obj["plan-price"]}`;

    const additionalList = document.querySelector('.additional-list');
    additionalList.innerHTML = '';

    let total = Number(obj["plan-price"].replace(/[^0-9]/g, ''));
    if (obj["online-service"]) {
        const additional = document.createElement('div');
        additional.classList.add('plan-additionals');
        const service = document.createElement('span');
        service.innerText = 'Online service';
        const price = document.createElement('span');
        price.classList.add('additional-price');
        price.innerText = `${obj["online-service"]}`;

        additional.appendChild(service);
        additional.appendChild(price);

        additionalList.appendChild(additional);

        total += Number(obj["online-service"].replace(/[^0-9]/g, ''));
    }
    if (obj["larger-storage"]) {
        const additional = document.createElement('div');
        additional.classList.add('plan-additionals');
        const service = document.createElement('span');
        service.innerText = 'Larger storage';
        const price = document.createElement('span');
        price.classList.add('additional-price');
        price.innerText = `${obj["larger-storage"]}`;

        additional.appendChild(service);
        additional.appendChild(price);

        additionalList.appendChild(additional);

        total += Number(obj["larger-storage"].replace(/[^0-9]/g, ''));
    }
    if (obj["customizable-profile"]) {
        const additional = document.createElement('div');
        additional.classList.add('plan-additionals');
        const service = document.createElement('span');
        service.innerText = 'Customizable profile';
        const price = document.createElement('span');
        price.classList.add('additional-price');
        price.innerText = `${obj["customizable-profile"]}`;

        additional.appendChild(service);
        additional.appendChild(price);

        additionalList.appendChild(additional);

        total += Number(obj["customizable-profile"].replace(/[^0-9]/g, ''));
    }

    // update total
    updateTotal(total);
}

function updateTotal(total) {
    const totalSpan = document.getElementById('total-value');
    if (obj["plan-type"] === 'monthly') {
        document.getElementById('total').innerText = 'Total (per month)';
        totalSpan.innerText = `$${total}/mo`;
    } else {
        document.getElementById('total').innerText = 'Total (per year)';
        totalSpan.innerText = `$${total}/yr`;
    }
}

function updatePlanDisplay(checked) {
    const planLabels = document.querySelectorAll('.lbl');
    planLabels.forEach((lbl) => {
        lbl.classList.remove('active');
    })
    if (checked) {
        planLabels[1].classList.add('active');
        displayYearlyPlans();
    } else {
        planLabels[0].classList.add('active');
        displayMonthlyPlans();
    }
}
function displayMonthlyPlans() {
    console.log('display monthly plans')
    const monthlyPrices = document.querySelectorAll('.monthly-price');
    const yearlyPrices = document.querySelectorAll('.yearly-price');
    const extraBenefits = document.querySelectorAll('.extra-benefits');
    yearlyPrices.forEach((element) => {
        element.style.display = 'none';
    })
    monthlyPrices.forEach((element) => {
        element.style.display = 'block';
    })
    extraBenefits.forEach((element) => {
        element.style.display = 'none';
    })
    obj["plan-type"] = 'monthly';
    console.log(obj);
}
function displayYearlyPlans() {
    const monthlyPrices = document.querySelectorAll('.monthly-price');
    const yearlyPrices = document.querySelectorAll('.yearly-price');
    const extraBenefits = document.querySelectorAll('.extra-benefits');
    yearlyPrices.forEach((element) => {
        element.style.display = 'block';
    })
    monthlyPrices.forEach((element) => {
        element.style.display = 'none';
    })
    extraBenefits.forEach((element) => {
        element.style.display = 'block';
    })
    obj["plan-type"] = 'yearly';
    console.log(obj);
}

function handleInfoSubmission() {
    const personalInfoForm = document.querySelector('.your-info');
    const selectPlanForm = document.querySelector('.select-plan');

    const name = document.getElementById('name').value;
    const email = document.getElementById('email').value;
    const phone = document.getElementById('phone').value;

    const isValid = validateInfo(name, email, phone);

    if (!isValid) {
        return;
    }

    obj.name = name;
    obj.email = email;
    obj.phone = phone;

    // hide the current form
    hideForm(personalInfoForm);
    // show next form + make monthly plan as default
    document.querySelector('.label-monthly').classList.add('active');
    showForm(selectPlanForm);
}

function handlePlanSubmission() {
    if (!obj["plan-price"]) {
        document.getElementById('plan-error').textContent = 'Please select a plan to proceed.';
        return;
    }
    const selectPlanForm = document.querySelector('.select-plan');
    const addOnsForm = document.querySelector('.add-ons');

    // hide the current form
    hideForm(selectPlanForm);
    // show next form
    showForm(addOnsForm);
}

function handleAddOnSubmission() {
    const onlineService = document.getElementById('online-service');
    const largerStorage = document.getElementById('larger-storage');
    const customizableProfile = document.getElementById('customizable-profile');

    if (onlineService.checked) {
        // grabbing the price of this service
        obj["online-service"] = onlineService.parentElement.parentElement.children[1].innerText;
    } else {
        obj["online-service"] = null;
    }
    if (largerStorage.checked) {
        obj["larger-storage"] = largerStorage.parentElement.parentElement.children[1].innerText;
    } else {
        obj["larger-storage"] = null;
    }
    if (customizableProfile.checked) {
        obj["customizable-profile"] = customizableProfile.parentElement.parentElement.children[1].innerText;
    } else {
        obj["customizable-profile"] = null;
    }

    const addOnForm = document.querySelector('.add-ons');
    const summaryForm = document.querySelector('.summary');

    updateSummary();

    // hide the current form
    hideForm(addOnForm);
    // show next form
    showForm(summaryForm);
}

function hideForm(form) {
    form.style.display = 'none';
}

function showForm(form, obj) {
    form.style.display = 'flex';
}

function validateInfo(name, email, phone) {
    const nameErrorMessage = document.getElementById('name-error');
    const emailErrorMessage = document.getElementById('email-error');
    const phoneErrorMessage = document.getElementById('phone-error');

    const nonAlphabeticRegex = /[^a-zA-Z]/;
    const nonNumericRegex = /[^0-9]/;

    let validFlag = true;

    if (!name) {
        validFlag = false;
        nameErrorMessage.textContent = 'This field is required.';
    } else if (name.trim().length < 1 || nonAlphabeticRegex.test(name.trim().replace(' ', ''))) {
        validFlag = false;
        nameErrorMessage.textContent = 'Name can only include letters (a-z).'
    } else {
        nameErrorMessage.textContent = '';
    }

    if (!email) {
        validFlag = false;
        emailErrorMessage.textContent = 'This field is required.';
    } else if (!email.includes('@')) {
        validFlag = false;
        emailErrorMessage.textContent = `Email must contain '@' symbol.`;
    } else {
        emailErrorMessage.textContent = '';
    }

    if (!phone) {
        validFlag = false;
        phoneErrorMessage.textContent = 'This field is required.';
    } else if (phone.trim().length !== 10 || nonNumericRegex.test(phone.trim())) {
        validFlag = false;
        phoneErrorMessage.textContent = 'Not a valid 10-digit IND phone number (must not include spaces or special characters).';
    } else {
        phoneErrorMessage.textContent = '';
    }

    return validFlag;
}